# A running list of todo items
...that apply to more than just a single file.

# format
@todo - <entry>


@todo - fix osx failure to tab-complete, maybe just burn down the OS and start over (again)?
@todo - combine program structure and documentation
@todo - create a newline automatic repeat for '@todo - '
@todo - definitions. ugh.
@todo - set up laminar artifact downloading
@todo - script to move all non-tracked files out of f.sh before killing a checkout completely
@todo - dependency list
@todo - build os switcher sed -i implmementation for the #!/path/to/bash line
@todo - ...compile bash itself?
@todo - auto-update golang, git, gitea, laminar, and all dependencies whenever anything in the chain gets an update
@todo - find my other to-do list(s), merge with this one (i know i had a really long one once upon a time. maybe on the tablet?
@todo - naming conventions
@todo - sort and group functions better (long process, ongoing)
@todo - breakfast, also vits&tea
@todo - write script to autoreplace WAN links with archive.is and internetarchive links, with generation/curlhash/httpcodeverification.
@todo - add kanban.bash to install process/prerequisites (https://github.com/coderofsalvation/kanban.bash)
@todo - programmatically display included & sourced files for current shell, along with $PATH and other environment variables.
