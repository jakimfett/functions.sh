# functions.sh
Several shell scripts stacked on top of eachother, attempting to be useful.

# assumptions
You solemnly swear you're up to no good.  
You understand that this is a work in progress.  
You will understand what something does before executing it.

# disclaimer
Shell scripts are dangerous.  
This one moreso than some.  
If you break something with a borrowed tool, that's on you.

This particular set of scripts is optimized for use inside the Bourne-Again shell v4.x (and above) on a 2019-current-ish Debian/Raspbian system, in a homelab.

You CentOS/RedHat/BeOS/BSD/Unix/etc experience may be debuging-laden.

Bugfix and other contributions welcome.

# license
© 2015-2022 @jakimfett
All rights reserved.
All material contained in this repository is licensed for use, redistribution, and modification under _Creative Commons 4.0 Attribution+ShareAlike ([CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/))._
