#!/bin/bash
# @author:'`whoami`'
# @license;'cc-by-sa'
#
# Bootstrap script to install a gitea repo on a system.

# files modified by this script:

# where are you getting your code?
repoSource='https://git.functions.sh/'
# have you considered mirroring?

# @function.desc
# A description of the function, briefly.
#
# @function.syntax
# 	`function "parameter"` <-- short(est?) version of the function command
#	`function "param" <optional>` <-- optional params use brackets.
#	`function "param" <ordered> <sequence> <2> <3>` <-- starts @zero
#
# @function.returns
#	0-2, 126-128, 130, 255+ = [linux standard](http://www.tldp.org/LDP/abs/html/exitcodes.html)
# 	n = custom return code
#
# @function.comment
#	Meta information on the function.
# 	This function is for the template, and exists primarily for informational purposes.
function templateFunction {
	exit 0
}
