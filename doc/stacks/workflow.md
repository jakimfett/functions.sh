Having an efficient workflow is important.

How important?

Depends on how often you do the task.  
Keep reading.

_(this is about @jakimfett's workflow, and may not apply to literally any other being)_
_(suggestions on optimizations are encouraged via pull request)_

# Concept

Large loops take a while to go around.

Slightly smaller loops take less time to go around, given a similar amount of effort.

Meaning, if I focus on the ratio between duration and frequency, I can determine if I'm optimizing the correct thing.
Meaning, if a task takes a long time, but you do it infrequently, it is more effecient than if the task is frequent and the time taken short.
Meaning, duration over frequency multiplied by effort-per-output.

# Practicum

I use a text terminal, called the `**B**ourne **A**gain **Sh**ell` or just `bash` for short, to accomplish other work.

My expectation is that this basic, job-agnostic tool is ready to be used whenever I need it.

Practically speaking, this is not the case.

## Workflow Idealism

Your current workflow is bad.

It's bad because technology has invaded it with unnecessary dependencies.  
Depending on other things is good, but continuing to function when the outside things are down is requrement of fault-tolerant systems.
More on this later.

### Ideal #1
The ideal workflow has relatively few distractions.

Context switching is expensive, and by focusing, we get more done with the same amount of resources.

#### Interruptions
A poorly timed interruption can destroy a carefully assembled mental model, framework, concept, or idea-in-progress.  

Due to the continued lack of readily available instant-replay of past mental state, an interruption can damage your forward progress.

The cost is not the hours.
The cost is the partially-reassembled thing that never gets to be.
Wandering is productive, even when a thought dies.

Use an interruption as a way to switch approaches, rather than re-tracing your steps.

#### Appropriate Distraction
Take time intentionally to care for your body's needs.  
Stay hydrated throughout the day, and keep an idea of when you've last showered and eaten.  

These things are necessary, and failing to do so will result in much decreased uptime.

Speaking of appropriate distraction, I need more tea.

Back in a jiffy.

### Ideal #2
The ideal workflow is accessible.

Having hurdles in your path to overcome before you can do what you do best will impede momentum and sap your energy prematurely.

The cost is what you could not accomplish due to energy expended on repetative, non-core tasks.
Talking with your facilitator if you have accessiblity needs, they will advocate for you.
Spoon-debt takes an extra toll on the vulnerable and week.

#### Inaccessibility
Declare something unachieveable.

Ask for someone to help you.  
Do so until the barrier is removed or they stop asking you to do the thing.

####
