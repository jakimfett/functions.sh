I use a tiling window manager, called `dvtm`.

# Commands
CTL+G, C			==>	new pane
CTL+G, ENTER	==>	shift selected pane to primary position
