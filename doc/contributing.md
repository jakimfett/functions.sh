So you wanna add to this festering mass of creative excellence, eh?
*pats the core code affirmingly*

*something falls off*

...just ignore that part.

# How Do
You're gonna want to understand [git branching & merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) as well as have a good idea of what you want to add.

Make a branch named after your addition:
`git checkout -b <feature-name>`

Eg, if you're doing something high level (like adding documentation for a new application), something like `appname-docs-addition` or `doc-new-app`

# Appendices

https://www.atlassian.com/git/tutorials/using-branches/git-merge
