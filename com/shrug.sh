#!/bin/bash
# @author:'jakimfett'
# @license;'cc-by-sa'
#
# ¯\_(ツ)_/¯


# @shrug.desc
# Prints an ascii 'shrug' emote.
#
# @shrug.syntax
# 	`shrug.sh` <-- prints the emote
#
# @shrug.returns
#	0
#
# @shrug.comment
#	This is an emote. 
#	There are many like it, but this one is a shrug.
#	May not communicate well with species that don't have shoulders.
function shrug {
	echo '¯\_(ツ)_/¯'
	exit 0
}

shrug
